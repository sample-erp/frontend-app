# optic-erp Frontend

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## Project build process with using npm

### Setup node.js and npm
```
sudo apt install nodejs
```
```
sudo apt install npm
```
### Setup vue-cli
```
npm install -g @vue/cli
```
Open project on your IDE (I use WebStorm). In the project directory run the first two commands.