import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';

Vue.use(Vuetify);

export default new Vuetify({
    theme:{
        themes: {
            light: {
                primary: '#673ab7',
                secondary: '#e91e63',
                accent: '#ff5722',
                error: '#f44336',
                warning: '#ff9800',
                info: '#3f51b5',
                success: '#4caf50'

            }
        }
    }

});
