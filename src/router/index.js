import Vue from 'vue'
import VueRouter from 'vue-router'
import Header from "../components/Header";
import Menu from "../components/Menu";
import Roles from "../components/Roles";
import Users from "../components/Users";
import Accounts from "../components/Accounts";
import Firms from "../components/Firms";
import FirmForm from "../components/FirmForm";
import FirmCard from "../components/FirmCard";
import i18n from "../i18n";
import AccountCard from "../components/AccountCard";
import AccountForm from "../components/AccountForm";
import Login from "../components/LoginForm";
import Logout from "../components/LogoutDialog";
import authStore from "../store/modules/authorisation";

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    components: {header: Header, menu: Menu},
    meta: {
      requiresAuth: true
    },
  },
  {
    name: 'login',
    path: '/home/login',
    component: Login,
    meta: {
      requiresAuth: false
    },
  },
  {
    name: 'roles',
    path: '/home/roles',
    component: Roles,
    meta: {
      requiresAuth: true
    }
  },
  {
    name: 'users',
    path: '/home/users',
    component: Users,
    meta: {
      requiresAuth: true
    }
  },
  {
    name: 'accounts',
    path: '/home/accounts',
    component: Accounts,
    meta: {
      requiresAuth: true
    }
  },
  {
    name: 'accountCard',
    path: '/home/accounts/:uuid',
    component: AccountCard,
    meta: {
      breadCrumb(route) {
        let currentAccount = this.$store.getters["accountsStore/getCurrentAccount"]
        return [
          {
            text: i18n.t('accounts.title'),
            to: { name: 'accounts' },
            link: true,
            exact: true,
            disabled: false,
          },
          {
            text: currentAccount.name,
          }
        ]
      }
    }
  },
  {
    name: 'addAccountForm',
    path: '/home/accounts/add',
    component: AccountForm,
    meta: {
      breadCrumb(route) {
        return [
          {
            text: i18n.t('accounts.title'),
            to: {name: 'accounts'},
            link: true,
            exact: true,
            disabled: false,
          },
          {
            text: i18n.t('global.breadcrumbs.add')
          }
        ]
      }
    }
  },
  {
    name: 'editAccountForm',
    path: '/home/accounts/:uuid/edit',
    component: AccountForm,
    meta: {
      breadCrumb(route) {
        let currentAccount = this.$store.getters["accountsStore/getCurrentAccount"]
        return [
          {
            text: i18n.t('accounts.title'),
            to: {name: 'accounts'},
            link: true,
            exact: true,
            disabled: false,
          },
          {
            text: currentAccount.name,
            to: {name: 'accountCard', params: currentAccount.uuid},
            link: true,
            exact: true,
            disabled: false,
          },
          {
            text: i18n.t('global.breadcrumbs.edit')
          }
        ]
      }
    }
  },
  {
    name: 'firms',
    path: '/home/firms',
    component: Firms,
    meta: {
      requiresAuth: true
    }
  },
  {
    name: 'firmCard',
    path: '/home/firms/:uuid',
    component: FirmCard,
    meta: {
      breadCrumb(route) {
        let currentFirm = this.$store.getters["firmsStore/getCurrentFirm"]
        return [
          {
            text: i18n.t('firms.title'),
            to: { name: 'firms' },
            link: true,
            exact: true,
            disabled: false,
          },
          {
            text: currentFirm.name,
          }
        ]
      }
    }
  },
  {
    name: 'addFirmForm',
    path: '/home/firms/add',
    component: FirmForm,
    meta: {
      breadCrumb(route) {
        return [
          {
            text: i18n.t('firms.title'),
            to: {name: 'firms'},
            link: true,
            exact: true,
            disabled: false,
          },
          {
            text: i18n.t('global.breadcrumbs.add')
          }
        ]
      }
    }
  },
  {
    name: 'editFirmForm',
    path: '/home/firms/:uuid/edit',
    component: FirmForm,
    meta: {
      breadCrumb(route) {
        let currentFirm = this.$store.getters["firmsStore/getCurrentFirm"]
        return [
          {
            text: i18n.t('firms.title'),
            to: {name: 'firms'},
            link: true,
            exact: true,
            disabled: false,
          },
          {
            text: currentFirm.name,
            to: {name: 'firmCard', params: currentFirm.uuid},
            link: true,
            exact: true,
            disabled: false,
          },
          {
            text: i18n.t('global.breadcrumbs.edit')
          }
        ]
      }
    }
  },
  {
    name: 'logout',
    path: '/home/logout',
    component: Logout
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

function isRequiresAuth(to) {
  return to.matched.some(record => record.meta.requiresAuth)
}

function isUserAuthenticated() {
  return (authStore.state.user || JSON.parse(localStorage.getItem('user')))
}

function routeToLoginForm(to, next) {
  if (to.name !== 'login') {
    next({
      name: 'login',
      query: {
        targetURL: to.fullPath
      }
    })
  }
}

router.beforeEach((to, from, next) => {
  if (!isRequiresAuth(to) || isUserAuthenticated()) {
    next()
  } else {
    routeToLoginForm(to, next)
  }
})

export default router
