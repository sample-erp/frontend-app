import axios from '../http-common.js'

export default function setAuthHeader(user) {

    if (user === undefined) {
        user = JSON.parse(localStorage.getItem('user'));
    }
    if (user && user.accessToken) {
        axios.defaults.headers.common['Authorization'] = `Bearer ${user.accessToken}`;
    } else {
        return {};
    }
}