import axios from '@/http-common.js'

const authStore = {
    namespaced: true,
    state: {
        user: null,
        logoutCheck: false
    },

    mutations: {
        loginSuccess(state, user) {
            state.user = user;
        },

        loginReset(state) {
            state.user = null;
            state.logoutCheck = false;
        },

        switchLogoutCheck(state) {
            state.logoutCheck = !state.logoutCheck;
        }
    },

    actions: {
        login({ commit }, [user, callback]) {
            axios.post('auth/signin', user).then(
                (response) => {
                    if (response.data.accessToken) {
                        localStorage.setItem('user', JSON.stringify(response.data))
                        axios.defaults.headers.common['Authorization'] = `Bearer ${response.data.accessToken}`;
                    }
                    commit('loginSuccess', user);
                    callback();
                    return Promise.resolve(user);
                },
                error => {
                    callback();
                    return Promise.reject(error);
                }
            );
        },
        logout({commit},  callback){
            commit('loginReset');
            localStorage.removeItem('user');
            delete axios.defaults.headers.common["Authorization"];
            callback();
        }
    },

    getters:{
        getLoggedIn(state) {
            return state.user;
        },

        getLogoutCheck(state) {
            return state.logoutCheck;
        }
    }
}

export default authStore;