import axios from '@/http-common.js'

const firmsStore = {
    namespaced: true,
    state: {
        firmFormAction: '',
        firms: [],
        totalElements: 0,
        currentFirm: {
            uuid : "",
            type : 0,
            name : "",
            fullName : "",
            firstName : "",
            lastName : "",
            patronymic : "",
            inn : "",
            kpp : "",
            jurAddress : "",
            postAddress : "",
            phones : "",
            email : "",
            managerFio : "",
            managerPosition : "",
        },
        currentFirmUsers: []
    },
    mutations: {
        setFirms(state, firms) {
            state.firms = firms;
        },

        setTotalFirms(state, page) {
            state.totalElements = page.totalElements;
        },

        setCurrentFirm(state, firm) {
            state.currentFirm = firm;
        },

        setCurrentFirmUsers(state, users) {
            state.currentFirmUsers = users;
        },

        setFirmFormAction(state, action) {
            state.firmFormAction = action;
        }
    },
    actions: {
        fetchFirms({commit}, {sortBy, sortDesc, page, itemsPerPage}) {
            axios.get(`/firms?size=${itemsPerPage}&page=${page}&sort=${sortBy},${sortDesc}`)
                .then((response) => {
                    let firms = response.data._embedded.firms.map( firm => {
                        return {
                            uuid: firm.uuid,
                            type: firm.type,
                            typeStr: firm.type === 1? 'ИП' : 'Юр.лицо',
                            name: firm.name,
                            fullName: firm.fullName,
                            firstName: firm.firstName,
                            lastName: firm.lastName,
                            patronymic: firm.patronymic,
                            inn: firm.inn,
                            kpp: firm.kpp,
                            jurAddress: firm.jurAddress,
                            postAddress: firm.postAddress,
                            phones: firm.phones,
                            email: firm.email,
                            managerFio: firm.managerFio,
                            managerPosition: firm.managerPosition,
                        }
                    })
                    commit('setFirms', firms);
                    commit('setTotalFirms', response.data.page);
                });
        },
        createFirm({dispatch}, firm) {
            axios.post('/firms', firm)
                .then((response) => {

                });
        },
        updateFirm({dispatch}, firm) {
            axios.put(`/firms/${firm.uuid}`, firm)
                .then((response) => {
                })
        },
        fetchCurrentFirm({commit}, id) {
            axios.get(`/firms/${id}`)
                .then((response) => {
                    commit('setCurrentFirm', response.data);
                })
        },

        fetchCurrentFirmUsers({commit}, id) {
            axios.get(`/firms/${id}/users`)
                .then((response) => {
                    commit('setCurrentFirmUsers', response.data._embedded.users);
                })
        },

        deleteFirm({dispatch}, id) {
            axios.delete(`/firms/${id}`)
                .then((response) => {
                })
        }
    },
    getters: {
        getFirms(state) {
            return state.firms;
        },

        getTotalElements(state) {
            return state.totalElements;
        },

        getCurrentFirm(state) {
            return state.currentFirm;
        },

        getCurrentFirmUsers(state) {
            return state.currentFirmUsers
        },

        getFirmFormAction(state) {
            return state.firmFormAction;
        },
    }
}

export default firmsStore;