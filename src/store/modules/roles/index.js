import axios from '@/http-common.js'

const rolesStore = {
    namespaced: true,
    state: {
        roles: [],
        totalElements: 0,
        currentRole:{
            name: '',
            description: '',
            id: ''
        },
    },
    mutations: {
        setRoles(state, roles) {
            state.roles = roles;
        },

        setTotalRoles(state, page) {
            state.totalElements = page.totalElements;
        },

        setCurrentRole(state, role) {
            state.currentRole = role;
        }
    },
    actions: {
        fetchRoles({commit}, {sortBy, sortDesc, page, itemsPerPage}) {
            axios.get(`/roles?size=${itemsPerPage}&page=${page}&sort=${sortBy},${sortDesc}`)
                .then((response) => {
                    commit('setRoles', response.data._embedded.roles);
                    commit('setTotalRoles', response.data.page);
                });
        },
        createRole({dispatch}, [role, {sortBy, sortDesc, page, itemsPerPage}]) {
            axios.post('/roles', role)
                .then((response) => {
                    dispatch('fetchRoles', {sortBy, sortDesc, page, itemsPerPage});
                });
        },
        updateRole({dispatch}, [role, {sortBy, sortDesc, page, itemsPerPage}]) {
            axios.put(`/roles/${role.id}`, role)
                .then((response) => {
                    dispatch('fetchRoles', {sortBy, sortDesc, page, itemsPerPage});
                })
        },
        deleteRole({dispatch}, [id, {sortBy, sortDesc, page, itemsPerPage}]) {
            axios.delete(`/roles/${id}`)
                .then((response) => {
                    dispatch('fetchRoles', {sortBy, sortDesc, page, itemsPerPage});
                })
        }
    },
    getters: {
        getRoles(state) {
            return state.roles;
        },

        getTotalElements(state) {
            return state.totalElements;
        },

        getCurrentRole(state) {
            return state.currentRole;
        }
    }
}

export default rolesStore;