import axios from '@/http-common.js'

const usersStore = {
    namespaced: true,
    state: {
        users: [],
        totalElements: 0,
        currentUser: {
            username: '',
            password: '',
            firstName: '',
            lastName: '',
            uuid: ''
        },
        userRoles: []
    },

    mutations: {
        setUsers(state, users) {
            state.users = users;
        },

        setTotalUsers(state, page) {
            state.totalElements = page.totalElements;
        },

        setUserRoles(state, roles) {
            state.userRoles = roles;
        },

        setCurrentUser(state, currentUser) {
            state.currentUser = currentUser
        }
    },

    actions: {

        fetchUsers( {commit}, {sortBy, sortDesc, page, itemsPerPage}) {
            axios.get(`/users?size=${itemsPerPage}&page=${page}&sort=${sortBy},${sortDesc}`)
                .then((response) => {
                    commit('setUsers', response.data._embedded.users);
                    commit('setTotalUsers', response.data.page);
                });
        },

        createUser({dispatch}, [user, {sortBy, sortDesc, page, itemsPerPage}]) {
            axios.post('/users', user)
                .then((response) => {
                    dispatch('putUserRoles', [response.data, {sortBy, sortDesc, page, itemsPerPage}])
                    dispatch('fetchUsers', {sortBy, sortDesc, page, itemsPerPage});
                });
        },

        updateUser({dispatch}, [user, {sortBy, sortDesc, page, itemsPerPage}]) {
            axios.put(`/users/${user.uuid}`, user)
                .then((response) => {
                    dispatch('fetchUsers', {sortBy, sortDesc, page, itemsPerPage});
                })
        },

        putUserRoles({dispatch, state}, [user, {sortBy, sortDesc, page, itemsPerPage}]) {
            let roles = "";
            for (let i = 0; i < state.userRoles.length; i++) {
                roles += state.userRoles[i].url + "\n";
            }
            axios.put(`/users/${user.uuid}/roles`, roles, {headers: {"Content-type": "text/uri-list"}})
                .then((response) => {
                    dispatch('fetchUsers', {sortBy, sortDesc, page, itemsPerPage});
                })
        },

        fetchUserRoles({commit}, user) {
            axios.get( `/users/${user.uuid}/roles`)
                .then((response) => {
                    let roles = response.data._embedded.roles.map( role => {
                        return {
                            value: role.id,
                            text: role.description,
                            name: role.name,
                            url: role._links.role.href
                        }
                    })
                    commit('setUserRoles', roles);
                })
        },

        deleteUser({dispatch}, [id, {sortBy, sortDesc, page, itemsPerPage}]) {
            axios.delete(`/users/${id}`)
                .then((response) => {
                    dispatch('fetchUsers', {sortBy, sortDesc, page, itemsPerPage});
                })
        }
    },

    getters: {
        getUsers(state) {
            return state.users;
        },

        getTotalElements(state) {
            return state.totalElements;
        },

        getUserRoles(state) {
            return state.userRoles;
        },

        getCurrentUser(state) {
            return state.currentUser;
        }
    }
}

export default usersStore;