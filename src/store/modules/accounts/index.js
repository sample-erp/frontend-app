import axios from '@/http-common.js'

const accountsStore = {
    namespaced: true,
    state: {
        accounts: [],
        totalElements: 0,
        currentAccount:{
            name: '',
            description: '',
            uuid: ''
        },
        currentAccountFirms: [],
        currentAccountUsers: [],
        accountFormAction: ''
    },
    mutations: {
        setAccounts(state, accounts) {
            state.accounts = accounts;
        },

        setTotalAccounts(state, page) {
            state.totalElements = page.totalElements;
        },

        setCurrentAccount(state, account) {
            state.currentAccount = account;
        },

        setCurrentAccountFirms(state, firms) {
            state.currentAccountFirms = firms;
        },

        setCurrentAccountUsers(state, users) {
            state.currentAccountUsers = users;
        },

        setAccountFormAction(state, action) {
            state.accountFormAction = action;
        }
    },
    actions: {
        fetchAccounts({commit}, {sortBy, sortDesc, page, itemsPerPage}) {
            axios.get(`/accounts?size=${itemsPerPage}&page=${page}&sort=${sortBy},${sortDesc}`)
                .then((response) => {
                    commit('setAccounts', response.data._embedded.accounts);
                    commit('setTotalAccounts', response.data.page);
                });
        },
        fetchCurrentAccount({commit}, id) {
            axios.get(`/accounts/${id}`)
                .then((response) => {
                    commit('setCurrentAccount', response.data);
                })
        },
        fetchCurrentAccountFirms({commit}, id) {
            axios.get(`/accounts/${id}/firms`)
                .then((response) => {
                    let firms = response.data._embedded.firms.map( firm => {
                        return {
                            uuid: firm.uuid,
                            type: firm.type,
                            typeStr: firm.type === 1? 'ИП' : 'Юр.лицо',
                            name: firm.name,
                            fullName: firm.fullName,
                            firstName: firm.firstName,
                            lastName: firm.lastName,
                            patronymic: firm.patronymic,
                            inn: firm.inn,
                            kpp: firm.kpp,
                            jurAddress: firm.jurAddress,
                            postAddress: firm.postAddress,
                            phones: firm.phones,
                            email: firm.email,
                            managerFio: firm.managerFio,
                            managerPosition: firm.managerPosition,
                        }
                    })
                    commit('setCurrentAccountFirms', firms);
                })
        },
        fetchCurrentAccountUsers({commit}, id) {
            axios.get(`/accounts/${id}/users`)
                .then((response) => {
                    commit('setCurrentAccountUsers', response.data._embedded.users);
                })
        },
        createAccount({dispatch}, account) {
            axios.post('/accounts', account)
                .then((response) => {

                });
        },
        updateAccount({dispatch}, account) {
            axios.put(`/accounts/${account.uuid}`, account)
                .then((response) => {
                })
        },
        deleteAccount({dispatch}, id) {
            axios.delete(`/accounts/${id}`)
                .then((response) => {
                })
        }
    },
    getters: {
        getAccounts(state) {
            return state.accounts;
        },

        getTotalElements(state) {
            return state.totalElements;
        },

        getCurrentAccount(state) {
            return state.currentAccount;
        },

        getCurrentAccountFirms(state) {
            return state.currentAccountFirms;
        },

        getCurrentAccountUsers(state) {
            return state.currentAccountUsers;
        },

        getAccountFormAction(state) {
            return state.accountFormAction;
        }
    }
}

export default accountsStore;