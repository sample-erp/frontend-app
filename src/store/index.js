import Vue from 'vue'
import Vuex from 'vuex'
import rolesStore from './modules/roles'
import usersStore from "./modules/users";
import accountsStore from "./modules/accounts";
import firmsStore from "./modules/firms";
import authStore from "./modules/authorisation";

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    showMenu: true
  },

  mutations: {
    switchMenu(state) {
      state.showMenu = !state.showMenu;
      return state.showMenu;
    }
  },

  actions: {

  },

  getters: {

  },

  modules: {
    rolesStore: rolesStore,
    usersStore: usersStore,
    accountsStore: accountsStore,
    firmsStore: firmsStore,
    authStore: authStore
  }
})
