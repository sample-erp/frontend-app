import i18n from "../i18n";

let globalProps = {
    data() {
        return {
            tableFooterProps: () => {
                return {
                    itemsPerPageText: i18n.t('global.table.footer.rowsPerPage'),
                    pageText:'{0}-{1} '+ i18n.t('global.table.footer.pageText') + ' {2}',
                    itemsPerPageAllText: i18n.t('global.table.footer.perPageAllText'),
                    itemsPerPageOptions: [5, 10, 15, 50]
                }
            }
        }
    }
}

export default globalProps;