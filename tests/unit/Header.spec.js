import Header from "@/components/Header";
import RU from '@/locales/ru.json'

import Vuetify from "vuetify";
import Vuex from "vuex";
import VueI18n from "vue-i18n";


import {createLocalVue, mount, shallowMount} from '@vue/test-utils'


const ru = { ru: RU }

const localVue = createLocalVue()

localVue.use(Vuex)
localVue.use(VueI18n)

describe("Header", () => {

    let mutations
    let actions
    let store
    let vuetify

    beforeEach(() => {
        vuetify = new Vuetify()
        actions = {
            switcher: jest.fn()
        }
        mutations = {
            switchMenu: jest.fn()
        }
        store = new Vuex.Store({
            actions,
            mutations
        })
    })
//тестирование работы интернационализации
    it('успешная смена локали', () => {
        const wrapper = mount(Header, {
            store,
            localVue,
            i18n: new VueI18n({
                locale: 'ru',
                fallbackLocale: 'ru',
                messages: ru}),
            vuetify
        })

//Не работает поиск нужной кнопки по id #locale. Пока не разобралась почему
        wrapper.find('#locale').trigger('click') //нажатие на кнопку открытия меню со списком локалей

        wrapper.find('#locale #en').trigger('click') //нажатие на кнопку с локалью английского языка


        expect(wrapper.vm.$i18n.locale).toBe('en');
    })

    //тестирование взаимодействия с Vuex
    it('вызывает действие по нажатию кнопки', () => {
        const wrapper = mount(Header, {
            store,
            i18n: new VueI18n({
                locale: 'ru',
                fallbackLocale: 'ru',
                messages: ru}),
            localVue,
            vuetify
        })

        wrapper.find('#nav-icon').trigger('click')
        expect(mutations.switchMenu).toHaveBeenCalled()
    })


})